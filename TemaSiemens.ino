int lastPressed=0;
bool intrerupator=false;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(10,INPUT);//buton
  pinMode(12,OUTPUT);//led
}

void loop() {
  // put your main code here, to run repeatedly:

  int nivelIluminare=0;//senzor lumina/sunet
  int  buton=digitalRead(10);
  delay(10);

  if(buton==1 && buton!=lastPressed)
  {
    intrerupator=!intrerupator;

  }

  if (intrerupator)
  {
    digitalWrite(12,HIGH);
    nivelIluminare=analogRead(0);
  }
  else
  {
    digitalWrite(12,LOW);
    nivelIluminare=0;
  }

  Serial.println(nivelIluminare);

  
  lastPressed=buton;
}
