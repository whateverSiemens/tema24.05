#include "..\..\..\include\spring\Application\MonoInput.h"
#include<iostream>
#include <qendian.h>
QAudioFormat MonoInput::getAudioFormat()
{
	return mAudioFormat;
}

QVector<double> MonoInput::vecGetData()
{
	return mSamples;
}

MonoInput::MonoInput(const double displayTime, const unsigned int sampleRate)
{
	dataLenght = displayTime*sampleRate;
	mAudioFormat.setChannelCount(1);
	mAudioFormat.setSampleRate(sampleRate);
	mAudioFormat.setSampleSize(16);
	mAudioFormat.setSampleType(QAudioFormat::SignedInt);
	mAudioFormat.setByteOrder(QAudioFormat::LittleEndian);
	mAudioFormat.setCodec("audio/pcm");

	mMaxAmplitude = 32767;

}


qint64 MonoInput::writeData(const char* data, qint64 maxlen)
{

	const auto *ptr = reinterpret_cast<const char*>(data);


	for (int i = 0; i < maxlen / 2; i++)
	{
		qint32 value = 0;
		value = qFromLittleEndian<qint16>(ptr);

		auto level = float(value)*(5. / mMaxAmplitude);
		mSamples.push_back(level);
		ptr += 2;
	}

	if (mSamples.size() > dataLenght)
		mSamples.remove(0, mSamples.size() - dataLenght);

	//std::cout << maxlen << " ";
	return maxlen;
}
