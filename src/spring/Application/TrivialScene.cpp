#include "spring\Application\TrivialScene.h"
#include <Windows.h>
#include <TrivialLibrary.h>

typedef double(__cdecl *CalcProcedure)(const unsigned int, const double);

Spring::TrivialScene::TrivialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
{
}

void Spring::TrivialScene::createScene()
{
	std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

	m_uMainWindow->setWindowTitle(QString(appName.c_str()));
	createGUI();
}

void Spring::TrivialScene::release()
{
}

void Spring::TrivialScene::createGUI()
{
	QMessageBox m_MessageBox;
	HINSTANCE hModule = LoadLibrary(TEXT("TrivialLibrary.dll"));
	
	CalcProcedure Calculare = (CalcProcedure)GetProcAddress(hModule, "Calc");

	unsigned int sampleRate;
	sampleRate=boost::any_cast<unsigned int>(m_TransientDataCollection["SampleRate"]);

	double displayTime;
	displayTime = boost::any_cast<double>(m_TransientDataCollection["DisplayTime"]);
	double x = Calculare(sampleRate, displayTime);
	m_MessageBox.setText(QString::number(x));
	m_MessageBox.exec();


}

Spring::TrivialScene::~TrivialScene()
{
	;
}
