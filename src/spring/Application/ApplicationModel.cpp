#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>
#include <spring\Application\TrivialScene.h>
const std::string initialSceneName = "InitialScene";
const std::string baseSceneName = "BaseScene";
const std::string trivialSceneName = "TrivialScene";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{
	}

	ApplicationModel::~ApplicationModel()
	{
		/*std::ofstream out("TransientData.txt");
		m_TransientData = m_Scenes[initialSceneName]->uGetTransientData();
		std::string afisare = boost::any_cast<std::string>(m_TransientData["ApplicationName"]);
		out << afisare << std::endl;
		int afisare1 = boost::any_cast<unsigned int>(m_TransientData["SampleRate"]);
		out << afisare1 << std::endl;
		double afisare2 = boost::any_cast<double>(m_TransientData["DisplayTime"]);
		out << afisare2 << std::endl;
		afisare1 = boost::any_cast<unsigned int>(m_TransientData["RefreshRate"]);
		out << afisare1 << std::endl;*/
	}

	void ApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);

		IScene* baseScene = new BaseScene(baseSceneName);
		m_Scenes.emplace(baseSceneName, baseScene);

		IScene* trivialScene = new TrivialScene(trivialSceneName);
		m_Scenes.emplace(trivialSceneName, trivialScene);

	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		//add initial values for all transient data 
		std::string applicationName;
		unsigned int sampleRate;
		double displayTime;
		unsigned int refreshRate;

		/*std::ifstream f("TransientData.txt");
		if (f)
		{
			f >> applicationName;
			f >> sampleRate;
			f >> displayTime;
			f >> refreshRate;
		}
		else
		{*/
			applicationName = "Undefined";
			sampleRate = 25600;
			displayTime = 0.1;

			refreshRate = 1;
		//}
		m_TransientData["ApplicationName"] = applicationName;
		m_TransientData["SampleRate"] = sampleRate;
		m_TransientData["DisplayTime"] = displayTime;
		m_TransientData["RefreshRate"] = refreshRate;

	}

}
