#include "..\..\..\include\spring\Application\BaseScene.h"
#include<time.h>
#include<iostream>
#include<qserialportinfo.h>

namespace Spring
{

	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));
		createGUI();
		srand(time(NULL));
		timer = new QTimer();

		serialPort = new QSerialPort(this);
		for each (const QSerialPortInfo &serialPortInfo in QSerialPortInfo::availablePorts())
		{
			if (serialPortInfo.hasProductIdentifier() && serialPortInfo.hasVendorIdentifier())
				serialPort->setPortName(serialPortInfo.portName());
		}

		serialPort->open(QSerialPort::ReadOnly);
		serialPort->setBaudRate(QSerialPort::Baud9600);
		serialPort->setDataBits(QSerialPort::Data8);
		serialPort->setFlowControl(QSerialPort::NoFlowControl);
		serialPort->setParity(QSerialPort::NoParity);
		serialPort->setStopBits(QSerialPort::OneStop);

		const double displayTime = boost::any_cast<double>(m_TransientDataCollection["DisplayTime"]);
		const unsigned int sampleRate = boost::any_cast<unsigned>(m_TransientDataCollection["SampleRate"]);
		xAxis2.clear();
		for (int i = 0; i < displayTime*sampleRate; i++)
		{
			xAxis2.push_back(i*( 1./ sampleRate));
		}

		QObject::connect(backButton, SIGNAL(released()), this, SLOT(mf_BackButton()));
		QObject::connect(startButton, SIGNAL(released()), this, SLOT(mf_StartTimer()));
		QObject::connect(stopButton, SIGNAL(released()), this, SLOT(mf_StopTimer()));
		QObject::connect(stopButton, SIGNAL(released()), this, SLOT(mf_CleanPlot()));
		QObject::connect(timer, SIGNAL(timeout()), this, SLOT(mf_PlotRandom()));
		QObject::connect(serialPort, SIGNAL(readyRead()), this, SLOT(mf_OnReadyRead()));
		

	}

	void BaseScene::release()
	{
		delete timer;
		delete centralWidget;
	}
	
	BaseScene::~BaseScene()
	{
	}
	void BaseScene::createGUI()
	{
		m_uMainWindow->resize(410, 288);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		verticalLayout_2 = new QVBoxLayout();
		verticalLayout_2->setSpacing(6);
		verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
		customPlot = new QCustomPlot(centralWidget);
		customPlot->setObjectName(QStringLiteral("customPlot"));

		customPlot->addGraph();
		customPlot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);

		customPlot2 = new QCustomPlot(centralWidget);
		customPlot2->setObjectName(QStringLiteral("customPlot2"));

		customPlot2->addGraph();
		customPlot2->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);


		verticalLayout_2->addWidget(customPlot);
		verticalLayout_2->addWidget(customPlot2);

		horizontalLayout = new QHBoxLayout();
		horizontalLayout->setSpacing(6);
		horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout->addItem(horizontalSpacer);

		startButton = new QPushButton(centralWidget);
		startButton->setObjectName(QStringLiteral("startButton"));

		horizontalLayout->addWidget(startButton);

		stopButton = new QPushButton(centralWidget);
		stopButton->setObjectName(QStringLiteral("stopButton"));

		horizontalLayout->addWidget(stopButton);

		backButton = new QPushButton(centralWidget);
		backButton->setObjectName(QStringLiteral("backButton"));

		horizontalLayout->addWidget(backButton);

		verticalLayout_2->addLayout(horizontalLayout);


		gridLayout->addLayout(verticalLayout_2, 2, 0, 1, 1);

		m_uMainWindow->setCentralWidget(centralWidget);

		startButton->setText(QApplication::translate("MainWindow", "Start", Q_NULLPTR));
		stopButton->setText(QApplication::translate("MainWindow", "Stop", Q_NULLPTR));
		backButton->setText(QApplication::translate("MainWindow", "Back", Q_NULLPTR));
	}

	void BaseScene::mf_BackButton()
	{

		const std::string c_szNextSceneName = "InitialScene";
		emit SceneChange(c_szNextSceneName);
	}

	
	void BaseScene::mf_PlotRandom()
	{
		/*QVector<double> xValues, yValues;

		for (int i = 0; i < 100; i++)
		{
			xValues.push_back(rand() % 100-1);
			yValues.push_back(rand() % 100-1);
		}*/
		

		QVector<double> yAxis = mMonoInput->vecGetData();
		//QVector<double> yAxis;
		

		customPlot->graph(0)->setData(xAxis, yAxisLight);
		customPlot->rescaleAxes();
		customPlot->yAxis->setRange(0,1300);
		
		customPlot->replot();
		yAxisLight.clear();
		xAxis.clear();
		i = 0;


		customPlot2->graph(0)->setData(xAxis2, yAxis);
		customPlot2->rescaleAxes();

		customPlot2->replot();
	}



	void BaseScene::mf_CleanPlot()
	{
		customPlot->graph(0)->data()->clear();
		customPlot->replot();
		customPlot2->graph(0)->data()->clear();
		customPlot2->replot();
	}

	void BaseScene::mf_StartTimer()
	{
		backButton->setEnabled(false);
		const double displayTime = boost::any_cast<double>(m_TransientDataCollection["DisplayTime"]);
		const unsigned int sampleRate = boost::any_cast<unsigned>(m_TransientDataCollection["SampleRate"]);

		mMonoInput = new MonoInput(displayTime,sampleRate);
		mAudioInput = new QAudioInput(mMonoInput->getAudioFormat());
		mMonoInput->open(QIODevice::WriteOnly);
		mAudioInput->start(mMonoInput);
		
		unsigned int refreshRate = boost::any_cast<unsigned>(m_TransientDataCollection["RefreshRate"]);
		refreshRate =1000/refreshRate;
		timer->start(refreshRate);

	}

	void BaseScene::mf_StopTimer()
	{
		mMonoInput->close();
		mAudioInput->stop();
		serialPort->close();
		timer->stop();
		backButton->setEnabled(true);
	}

	void BaseScene::mf_OnReadyRead()
	{
		QByteArray data;
		QString aux = 0;
		while (serialPort->canReadLine())
		{
			data = serialPort->readLine();
			aux = data.trimmed();
			yAxisLight.push_back(aux.toDouble());
			xAxis.push_back(10*i);
			i++;
		}
	}


}
