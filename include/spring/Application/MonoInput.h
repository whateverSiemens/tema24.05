#include <qiodevice.h>
#include<qaudioformat.h>
#include <qvector.h>


class MonoInput : public QIODevice
{

private:
	QAudioFormat mAudioFormat;
	QVector<double> mSamples;
	qint32 mMaxAmplitude;
	double dataLenght;

public:
	qint64 QIODevice::readData(char* data, qint64 maxlen) override
	{
		Q_UNUSED(data);
		Q_UNUSED(maxlen);

		return -1;
	}
	qint64 QIODevice::writeData(const char* data, qint64 maxlen);
	QAudioFormat getAudioFormat();
	QVector<double> vecGetData();


	MonoInput(const double, const unsigned int);

};