#pragma once
#include <spring\Framework\IScene.h>
#include <QtWidgets/QApplication>
#include <qmessagebox.h>
#include <TrivialLibrary.h>
namespace Spring
{
	class TrivialScene : public IScene
	{

	public:

		explicit TrivialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;
		void createGUI();
		~TrivialScene();
	private:
	};

}
