#pragma once
#include <spring\Framework\IScene.h>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <qtimer.h>
#include <qcustomplot.h>
#include<spring\Application\MonoInput.h>
#include <qaudioinput.h>
#include "qserialport.h"
namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;
		void createGUI();
		~BaseScene();
	private:
		QWidget *centralWidget;
		QGridLayout *gridLayout;
		QVBoxLayout *verticalLayout_2;
		QCustomPlot *customPlot,*customPlot2;
		QHBoxLayout *horizontalLayout;
		QSpacerItem *horizontalSpacer;
		QPushButton *startButton;
		QPushButton *stopButton;
		QPushButton *backButton;

		QTimer *timer;
		MonoInput *mMonoInput;
		QAudioInput *mAudioInput;
		QSerialPort *serialPort;

		QVector<double> xAxis, yAxisLight,xAxis2;
		int i = 0;


	public slots:
		void mf_BackButton();
		void mf_PlotRandom();
		void mf_CleanPlot();
		void mf_StartTimer();
		void mf_StopTimer();
		void mf_OnReadyRead();

	};

}
